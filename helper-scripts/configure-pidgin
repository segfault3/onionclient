#!/usr/bin/env python3

import dbus
import socks
import sys
import time
import subprocess
import logging

from onionclient import _


class ConfigurationFailedError(Exception):
    user_visible = False


class RegisteringAccountFailedError(ConfigurationFailedError):
    user_visible = True


class ConnectingToDbusFailedError(ConfigurationFailedError):
    user_visible = True


class AccountNotConnectingError(ConfigurationFailedError):
    user_visible = True


class CancelledByUserError(ConfigurationFailedError):
    user_visible = False


# Pidgin has a PurpleAccountRegister DBus method that we could use,
# but it's messy: it opens a prompt that asks for which username to
# register but we have already provided it so this is just
# confusing. In fact, whatever is entered there is ignored. Even if
# this bug is fixed, it's hard to get feedback about how the
# registration process went via DBus, so it seems like a dead-end
# approach.
def register_xmpp_account(domain, port, username, password):
    # Under the expected circumstances we'll never receive more data
    # than this.
    MAX_RECV_LENGTH = 4096

    s = socks.socksocket()
    s.set_proxy(socks.SOCKS5, "127.0.0.1", 9050)
    try:
        s.connect((domain, port))
    except socks.ProxyError:
        raise RegisteringAccountFailedError(
            _("Could not connect to server %s:%s. Please check your network connection and that "
              "the server is running."))


    # The situation for XMPP libraries for python and other scripting
    # languages we ship in Tails, like perl, is terrible. python-xmpp
    # (xmppy) is unmaintained since 10 years back and is not available
    # for python 3; python3-nbxmpp does not have SOCKS support, and
    # the way it opens sockets and/or resolves hostnames is not
    # compatible with torsocks; libnet-xmpp-perl does not support
    # registering; libnet-jabber-perl seems completely broken; etc...
    # So, let's start implementing the XMPP protocol... :)
    s.sendall(bytes(
        """
        <stream:stream xmlns="jabber:client" to="{domain}" version="1.0"
         xmlns:stream="http://etherx.jabber.org/streams">
        """.format(domain = domain),
        'utf-8'
    ))
    # Make sure in-band registering is available (also implies that we
    # initiated the handshake successfully)
    resp = str(s.recv(MAX_RECV_LENGTH), 'utf-8')
    assert("<register xmlns='http://jabber.org/features/iq-register'/>" in resp)
    s.sendall(bytes(
        """
        <iq to="{domain}" type="set" id="1">
          <query xmlns="jabber:iq:register">
          <username>{username}</username>
          <password>{password}</password>
          </query>
        </iq>
        """.format(domain = domain, username = username, password = password),
        'utf-8'
    ))
    resp = str(s.recv(MAX_RECV_LENGTH), 'utf-8')
    # If the account already is registered, let's treat it as a
    # success (which might be wrong in some interpretations, since the
    # passwords might not match).
    if 'The requested username already exists' in resp:
        return True

    if resp == "<iq id='1' type='result' from='{domain}'/>".format(domain = domain):
        return True


def wait_for_pidgin(bus, purple_service):
    # In case this script is started too early vs Pidgin, let's give
    # the DBus service some time to start up.
    timeout = 10  # seconds
    start = time.time()
    while not bus.name_has_owner(purple_service):
        if time.time() > start + timeout:
            raise ConnectingToDbusFailedError(
                _("Timed out while waiting for Pidgin's DBus service"))
        time.sleep(0.1)


def create_account(purple_bus, domain, jid, password, port, protocol, username):
    account = purple_bus.PurpleAccountNew(jid, protocol)
    account_registered = register_xmpp_account(domain, port, username, password)
    if not account_registered:
        purple_bus.PurpleAccountDestroy(account)
        raise RegisteringAccountFailedError(_("Failed to register the Pidgin account"))
    purple_bus.PurpleAccountSetAlias(account, username)
    purple_bus.PurpleAccountSetBool(account, 'port', port)
    purple_bus.PurpleAccountSetPassword(account, password)
    purple_bus.PurpleAccountSetRememberPassword(account, 1)
    # The XMPP server doesn't do SSL, but we have onion transport
    # encryption so we're good.
    purple_bus.PurpleAccountSetBool(account, 'auth_plain_in_clear', 1)
    purple_bus.PurpleAccountSetString(account, 'connection_security',
                                  'opportunistic_tls')
    # Let's not involve any file transfer proxies. I'd like to
    # set it to the empty string, but this leads to a segfault
    # so a space will do.
    purple_bus.PurpleAccountSetString(account, 'ft_proxies', ' ')
    purple_bus.PurpleAccountsAdd(account)
    return account


def connect_account(purple_bus, account):
    if purple_bus.PurpleAccountGetEnabled(account, 'gtk-gaim'):
        if not purple_bus.PurpleAccountIsConnected(account):
            purple_bus.PurpleAccountConnect(account)
    else:
        purple_bus.PurpleAccountSetEnabled(account, 'gtk-gaim', 1)

    wait_until_account_connected(account, purple_bus)


def wait_until_account_connected(account, purple_bus):
    timeout = 10  # seconds
    start = time.time()
    while not purple_bus.PurpleAccountIsConnected(account):
        if time.time() > start + timeout:
            raise AccountNotConnectingError(
                _("Timed out while waiting for Pidgin account to connect"))
        time.sleep(0.1)


def join_chatroom(purple_bus, jid, account, chatroom, conference_domain):
    data = {
        "account": jid,
        "room": chatroom,
        "server": conference_domain
    }

    connection = purple_bus.PurpleAccountGetConnection(account)
    purple_bus.ServJoinChat(connection, data)


def add_chatroom_to_buddy_list(purple_bus, account, chatroom, domain, conference_domain):
    alias = domain
    url = chatroom + "@" + conference_domain
    if not purple_bus.PurpleBlistFindChat(account, url):
        purple_bus.PurpleBlistRequestAddChat(account, 0, alias, url)


def get_username_and_password(text=None):
        command = ["zenity", "--title", _("Create XMPP Account"), "--forms",
                   "--add-entry", _("Username"), "--add-password", _("Password"),
                   "--add-password", _("Confirm Password"), "--separator=\n"]
        if text:
            command.append("--text=" + text)
        else:
            command.append("--text=" + _("Choose a username and password"))

        p = subprocess.run(command, stdout=subprocess.PIPE)
        if p.returncode != 0:
            raise CancelledByUserError("User cancelled creating XMPP account")

        l = p.stdout.split()
        if len(l) != 3:
            logging.error("Couldn't parse username and password data: %r" % p.stdout)
            return get_username_and_password(_("Input was invalid"))

        username, password, confirmed_password = l

        if not username:
            return get_username_and_password(_("The username was empty. Try again"))

        if not password:
            return get_username_and_password(_("The password was empty. Try again"))

        if password != confirmed_password:
            return get_username_and_password(_("The passwords didn't match. Try again"))

        return username.decode(), password.decode()


def find_account_for_domain(purple_bus, domain, protocol):
    def find_jid():
        accounts = purple_bus.PurpleAccountsGetAll()
        for account in accounts:
            jid = purple_bus.PurpleAccountGetUsername(account)
            jid_domain = jid.split("@")[-1].split("/")[0]
            if jid_domain == domain:
                return jid

    jid = find_jid()
    if not jid:
        return None, None

    account = purple_bus.PurpleAccountsFind(jid, protocol)
    return account, jid


def configure(chatroom, domain, port):
    conference_domain = "conference." + domain
    protocol = 'prpl-jabber'
    purple_service = 'im.pidgin.purple.PurpleService'
    purple_object_path = '/im/pidgin/purple/PurpleObject'
    bus = dbus.SessionBus()
    wait_for_pidgin(bus, purple_service)
    purple_bus = bus.get_object(purple_service, purple_object_path)
    account, jid = find_account_for_domain(purple_bus, domain, protocol)
    if not account:
        username, password = get_username_and_password()
        jid = username + '@' + domain
        account = create_account(purple_bus, domain, jid, password, port, protocol, username)
    connect_account(purple_bus, account)
    if chatroom:
        join_chatroom(purple_bus, jid, account, chatroom, conference_domain)
        add_chatroom_to_buddy_list(purple_bus, account, chatroom, domain, conference_domain)


def display_error_message(message):
    command = ["zenity", "--error",
               "--title", _("Connecting to Tails Server failed"),
               "--text", message]
    subprocess.run(command, stdout=subprocess.PIPE)


def main():
    domain = sys.argv[1]
    port = int(sys.argv[2])
    if len(sys.argv) > 3:
        chatroom = sys.argv[3]
    else:
        chatroom = None

    try:
        configure(chatroom, domain, port)
    except ConfigurationFailedError as e:
        if e.user_visible:
            display_error_message(str(e))
        raise


if __name__ == "__main__":
    main()
