import subprocess
import os

from onionclient import _
from onionclient.launcher_template import ClientLauncher, ClientLauncherDetail
from onionclient.config import HELPER_SCRIPTS_DIR

CREATE_CONFIG_SCRIPT = os.path.join(HELPER_SCRIPTS_DIR, "create-gobby-config")


class GobbyLauncher(ClientLauncher):
    name = "gobby"
    icon = "gobby-0.5"

    @property
    def details(self):
        return super().details + [
            ClientLauncherDetail("password", _("Password"), str, required=False),
        ]

    def prepare(self):
        super().prepare()
        if "password" not in self.values:
            self.values["password"] = ""
        self.create_config_file()

    @staticmethod
    def create_config_file():
        subprocess.check_call(CREATE_CONFIG_SCRIPT)

    def launch(self):
        super().launch()
        # XXX: The connection string is user controlled, but because subprocess
        # handles escaping and quoting of arguments, this should still be secure.
        subprocess.Popen(["torsocks", "gobby", "infinote://%s:%s" % (self.values["address"], self.values["port"])])


launcher_class = GobbyLauncher
