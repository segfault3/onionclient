import subprocess
import os

from onionclient import _
from onionclient.launcher_template import ClientLauncher, ClientLauncherDetail
from onionclient.config import HELPER_SCRIPTS_DIR


class PidginLauncher(ClientLauncher):
    name = 'pidgin'
    icon = 'pidgin'

    @property
    def details(self):
        return super().details + [
            ClientLauncherDetail('chatroom', _('Chatroom'), str, required=False)
        ]

    @staticmethod
    def configure_pidgin(domain, port, chatroom):
        configure_pidgin_script = os.path.join(
            HELPER_SCRIPTS_DIR, 'configure-pidgin'
        )

        command = [configure_pidgin_script, domain, port]

        if chatroom:
            command.append(chatroom)

        subprocess.run(command)

    def prepare(self):
        super().prepare()
        if 'chatroom' not in self.values:
            self.values['chatroom'] = None

    def launch(self):
        super().launch()

        subprocess.Popen(['torsocks', 'pidgin'])
        self.configure_pidgin(self.values['address'], self.values['port'], self.values['chatroom'])


launcher_class = PidginLauncher
