import subprocess
import logging
import os
import re

from onionclient import _
from onionclient.launcher_template import ClientLauncher, ClientLauncherDetail, InvalidArgumentError
from onionclient.config import HELPER_SCRIPTS_DIR

ADD_KNOWN_HOST_SCRIPT = os.path.join(HELPER_SCRIPTS_DIR, "add-ssh-known-host")


class NautilusLauncher(ClientLauncher):
    name = "nautilus"
    name_for_display = "Files"
    icon = "network"
    user_name = "sftp"

    @property
    def details(self):
        return super().details + [
            ClientLauncherDetail("password", _("Password"), str, required=False),
            ClientLauncherDetail("key", _("SSH Public Key"), str, required=False),
        ]

    def check_if_details_sane(self):
        super().check_if_details_sane()
        # Can't sanity check the password, since we don't want to restrict it
        if "key" in self.values and not self.is_valid_key(self.values["key"]):
            raise InvalidArgumentError("SSH Public Key", self.values["key"])

    @staticmethod
    def is_valid_key(key):
        return re.match("^" + "[A-Za-z0-9-]+ [A-Za-z0-9+/=]+$", key)

    def prepare(self):
        super().prepare()
        if "key" in self.values:
            self.add_key_to_known_hosts()
        if "password" in self.values:
            self.add_ssh_password_to_keyring()

    def add_key_to_known_hosts(self):
        known_hosts_line = "%s %s" % (self.values["address"], self.values["key"])
        command = [ADD_KNOWN_HOST_SCRIPT, known_hosts_line]
        logging.info("Executing %r", " ".join(command))
        subprocess.run(command)

    def add_ssh_password_to_keyring(self):
        server = self.values["address"]
        port = self.values["port"]
        password = self.values["password"].encode()
        command = [
            "secret-tool", "store",
               "--collection=/org/freedesktop/secrets/collection/tails",
               "--label={}@{}:{}".format(self.user_name, server, port),
               "authtype", "password",
               "protocol", "sftp",
               "user", self.user_name,
               "server", server,
               "port", port,
               "xdg:schema", "org.gnome.keyring.NetworkPassword"
        ]
        logging.info("Executing %r", " ".join(command))
        subprocess.run(command, input=password)

    def launch(self):
        super().launch()
        # XXX: The connection string is user controlled, but because subprocess
        # handles escaping and quoting of arguments, this should still be secure.
        url = "sftp://%s@%s:%s/files" % (self.user_name, self.values["address"], self.values["port"])
        command = ["torsocks", "nautilus", url]
        logging.info("Executing %r", " ".join(command))
        subprocess.Popen(command)


launcher_class = NautilusLauncher
