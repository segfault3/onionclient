import subprocess

from onionclient.launcher_template import ClientLauncher

class TorBrowserLauncher(ClientLauncher):
    name = "tor-browser"
    icon = "tor-browser"

    def prepare(self):
        super().prepare()

    def launch(self):
        super().launch()
        # XXX: The connection string is user controlled, but because subprocess
        # handles escaping and quoting of arguments, this should still be secure.
        subprocess.Popen(["tor-browser", "%s:%s" %(self.values["address"], self.values["port"])])


launcher_class = TorBrowserLauncher
