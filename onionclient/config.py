from os import path
PACKAGE_PATH = path.dirname(path.realpath(__file__))

APP_NAME = "onionclient"
WINDOW_TITLE = "Connect to Onion Service"

DATA_DIR = "/usr/share/%s/" % APP_NAME
CLIENT_LAUNCHERS_DIR = path.join(DATA_DIR, "launchers")
HELPER_SCRIPTS_DIR = path.join(DATA_DIR, "helper-scripts")
UI_DIR = path.join(DATA_DIR, "ui")
CLIENT_UI_FILE = path.join(UI_DIR, "client_app.ui")
QUESTION_DIALOG_UI_FILE = path.join(UI_DIR, "question_dialog.ui")
ICON_DIR = path.join(DATA_DIR, "icons")
